﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	// Use this for initialization
	public GameObject enmbullet;
	bool move;
	float way;
	float random;

	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {
		Vector3 enemy = this.transform.position;
		if (move == true) {
			enemy.x += 1 * Time.deltaTime;
			way += 1 * Time.deltaTime;
			if (way >= 2) {
				move = false;
				enemy.y -= 1;
				way = 0;
			}
		} else if (move == false) {
			enemy.x -= 1 * Time.deltaTime;
			way += 1 * Time.deltaTime;
			if (way >= 2) {
				move = true;
				enemy.y -= 1;
				way = 0;
			}
		}
		this.transform.position = enemy;

		random += Time.deltaTime;
		if (random >= 1f) {
			int att = Random.Range (0, 5);
			if (att == 0) {
				Vector3 bullet = this.transform.position;
				bullet.y -= 1;
				Instantiate (enmbullet,bullet, Quaternion.identity);
			}
			random = 0;
		}
	}
}





