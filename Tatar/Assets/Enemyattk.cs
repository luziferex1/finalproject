﻿using UnityEngine;
using System.Collections;

public class Enemyattk : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		Vector3 shoot = this.transform.position;
		shoot.y -= 4f * Time.deltaTime;
		this.transform.position = shoot;
	}

	void OnCollisionEnter (Collision other){
		if (other.gameObject.tag != "Enemy") {
			Destroy (other.gameObject);
			Destroy (this.gameObject);
		}
	}
}
