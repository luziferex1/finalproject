﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	// Use this for initialization
	public GameObject Bull;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.A)) {
			Vector3 slide = this.transform.position;
			slide.x -= 5 * Time.deltaTime;
			this.transform.position = slide;
		}
		if (Input.GetKey (KeyCode.D)) {
			Vector3 slide = this.transform.position;
			slide.x += 5 * Time.deltaTime;
			this.transform.position = slide;
		}
		if (Input.GetKeyDown (KeyCode.Space)) {
			Vector3 startbull = this.transform.position;
			startbull.y = -3.4f;
			Instantiate (Bull, startbull, Quaternion.identity);
		}
	}
}
