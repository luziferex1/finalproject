﻿using UnityEngine;
using System.Collections;

public class PlayerBullet : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 Player = this.transform.position;
		Player.y += 10 * Time.deltaTime;
		this.transform.position = Player;
	}

	void OnCollisionEnter (Collision other){
		Destroy (other.gameObject);
		Destroy (this.gameObject);
	}
}
